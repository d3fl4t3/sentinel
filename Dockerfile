FROM python:3-onbuild
ADD . /usr/src/app
VOLUME /usr/src/app/database
CMD python3 ./bin/sentinel.py -s /sibcoin.conf && sleep 60